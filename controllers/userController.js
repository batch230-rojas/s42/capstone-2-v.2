const User = require("../models/user.js");
const Product = require("../models/product.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

module.exports.registerUser = (reqBody) => {
	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user,error) =>{
		if(error){
			return false;
		}
		else{
			return newUser;
		}
	})
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({email:reqBody.email}).then(result => {
		if(result == null){
			return false
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)};
			}
			else{
				return false;
			}
		}
	})
}

module.exports.getProfile = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	console.log(userData);

	return User.findById(userData.id).then(result => {
		result.password = "";
		response.send(result);
	})
}


module.exports.checkout = async (request, response) =>{

	const userData = auth.decode(request.headers.authorization);

let productName = await Product.findById(request.body.productId).then(result => result.name);

	let newOrder = {
		userId: userData.id,
		email: userData.email,
		productId:request.body.productId,
		productName: productName,
		quantity: request.body.quantity,
		price: request.body.price,
		totalAmount: (request.body.price * request.body.quantity)
	}
	
	response.send (newOrder);
	

	let isUserUpdated = await User.findById(data.userId)
	.then(user =>{
		user.cart.push({
			productId: data.productId,
			productName: data.productName
		});

		return user.save()
		.then(result =>{
			console.log(result);
			return true;
		})
		.catch(error =>{
			console.log(error);
			return false;
		})
	})

	console.log(isUserUpdated);

	let isProductUpdated = await Product.findById(data.productId).then(product =>{

		product.order.push({
			userId: data.userId,
			email: data.email
		})

		product.stocks -= 1;

		return product.save()
		.then(result =>{
			console.log(result);
			return true;
		})
		.catch(error =>{
			console.log(error);
			return false;
		})
	})

	console.log(isProductUpdated);

	(isUserUpdated && isProductUpdated) ? res.send(true) : res.send(false)

}


