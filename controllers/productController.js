const mongoose = require("mongoose");
const Product = require ("../models/product.js");

module.exports.addProduct = (reqBody, newData) => {
	if(newData.isAdmin == true){
		let newProduct = new Product({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price,
			stocks: reqBody.stocks
		})
		return newProduct.save().then((newProduct, error)=>{
			if(error){
				return false;
			}
			return newProduct;
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to create and add this product.');
		return message.then((value) => {return value});
	}
}

module.exports.getActiveProduct = () => {
	return Product.find({isActive:true}).then(result =>{
		return result;
	})
}

module.exports.getProduct = (req, res) =>{
	Product.findById(req.params.productId).then(result =>{
		res.send (result);
	})
}


module.exports.updateProduct = (productId, newData) => {
	if(newData.isAdmin == true){
		return Product.findByIdAndUpdate(productId,
			{
				price: newData.product.price,
				stocks: newData.product.stocks
			}
		).then((result, error)=>{
			if(error){
				return false;
			}
			return ("Product has been updated.")
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to update this product.');
		return message.then((value) => {return value});
	}
}

module.exports.productToArchive = (productId, archiveProduct) => {
	if(archiveProduct.isAdmin == true){
		return Product.findByIdAndUpdate(productId,
			{
				isActive: archiveProduct.product.isActive
			}
		).then((result, error)=>{
			if(error){
				return false;
			}
			return ("Product has been archived.")
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to archive this product.');
		return message.then((value) => {return value});
	}
}



