const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");
const auth = require("../auth.js");

router.post("/register", (request, response) =>{
	userController.registerUser(request.body).then(resultFromController => response.send(resultFromController));
})

router.post("/login", (request, response) => {
	userController.loginUser(request.body).then(resultFromController => response.send(resultFromController))
})

router.get("/details", auth.verify, userController.getProfile);

router.post("/checkout", auth.verify, userController.checkout);

module.exports = router; 

 
 