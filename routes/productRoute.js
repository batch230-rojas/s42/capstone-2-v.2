const express = require("express");
const router = express.Router();
const productController =  require("../controllers/productController.js");
const auth = require("../auth.js");

router.post("/create", auth.verify, (request,response) => 
{
	const newData = {
		product: request.body, 
		isAdmin: auth.decode(request.headers.authorization).isAdmin 
	}

	productController.addProduct(request.body, newData).then(resultFromController => {
		response.send(resultFromController)
	})
})

router.get("/active", (request, response) => {
	productController.getActiveProduct().then(resultFromController => response.send(resultFromController))
});

router.get("/:productId", productController.getProduct);

router.patch("/:productId/update", auth.verify, (request,response) => 
{
	const newData = {
		product: request.body, 
		isAdmin: auth.decode(request.headers.authorization).isAdmin 
	}

	productController.updateProduct(request.params.productId, newData).then(resultFromController => {
		response.send(resultFromController)
	})
});

router.patch("/:productId/archive", auth.verify, (request,response) => 
{
	const archiveProduct = {
		product: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin 
	}

	productController.productToArchive(request.params.productId, archiveProduct).then(resultFromController => {
		response.send(resultFromController)
	})
})

module.exports = router;